import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

class EmailClient:
    """Class responsible for email sending"""
    def __init__(self, email, password, port, server, receiver, logging):
        self.email = email
        self.password = password
        self.port = port
        self.server = server
        self.receiver = receiver
        self.logging = logging

    
    def send_email(self, tickets):
        """Function that actually sends email"""
        full_body = self._construct_body(tickets)

        msg = MIMEMultipart()
        msg['From'] = self.email
        msg['To'] = self.receiver
        msg['Subject'] = "We found %s new tickets" % len(tickets)

        msg.attach(MIMEText(full_body, 'html'))
        try:
            self.logging.info('Sending email with new tickets to %s' % self.receiver)
            server = smtplib.SMTP(self.server, self.port)
            server.starttls()
            server.login(self.email, self.password)
            text = msg.as_string()
            server.sendmail(self.email, self.receiver, text)
            server.quit()
        except Exception as e:
            self.logging.error('Failed to send Notification. Error: %s' % e)

    
    def _construct_body(self, tickets):
        """Helper function that constructs email body from tickets"""
        tmp_body = """
        <h2>Block ID: %s</h2>
        <p><strong>Quantity:</strong> %s</p>
        <p><strong>Price:</strong> %s</p>
        <p><strong>Section:</strong> %s</p>
        <p><strong>Row:</strong> %s</p>
        <p><strong>BUY:</strong> <a href="%s">%s</a></p>
        <hr>
        """
        full_body = ""
        for ticket in tickets:
            full_body += tmp_body % (ticket.block_id,
                                     ticket.quantity,
                                     ticket.price,
                                     ticket.section,
                                     ticket.row,
                                     ticket.buy_link,
                                     ticket.buy_link)
        return full_body
