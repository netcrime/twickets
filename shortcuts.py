import os
import sys
from datetime import datetime
import functools
from time import sleep
import inspect
import random

import requests

# for retrieving exception info
from traceback import format_exc as stacktrace


# name of main script (strips .py from filename)
APP = os.path.basename(sys.argv[0]).rsplit('.py', 1)[0] if sys.argv else None

# starting working directory
SWD = os.getcwd()

# shortcut
paths = os.path.join

# create directory if not exists
makedir = functools.partial(os.makedirs, exist_ok=True)


def readlines(file, mode='r', encoding=None, **kwargs):
    """:return array of lines"""
    with open(file, mode=mode, encoding=encoding, **kwargs)as f:
        return f.read().splitlines()


# *** datetime shortcuts ***
def time_now(fmt=None):
    d = datetime.now()
    return d.strftime(fmt) if fmt else d


def timestamp(fmt='%Y-%m-%d %H:%M:%S'):
    return datetime.now().strftime(fmt)


# *** custom log function generator ***
def logger(name=APP, sep=' ', end='\n\n', encoding='utf-8', ts_fmt='%Y-%m-%d', directory=paths('data', 'logs')):
    """
    :param name: name of logfile
    :param sep: default print arg
    :param end: default print arg
    :param encoding: with which to open file
    :param ts_fmt: timestamp format on logfile
    :param directory: where to save logfile
    :return: custom log function with ^ settings
    """

    # create logs-directory (relative directory path gets placed in application's start-directory)
    folder = directory if os.path.isabs(directory) else paths(SWD, directory)
    makedir(folder)

    ts = None  # create new file when timestamp changes
    file = None  # file handle

    def func(*values, sep=sep, end=end):
        nonlocal ts, file

        now = timestamp(ts_fmt)

        if ts != now:
            if file:
                file.close()

            # open new logfile
            file = open(paths(folder, "{}--{}.log".format(name, now)), 'a+', encoding=encoding)
            ts = now

        print(*values, sep=sep, end=end, file=file, flush=True)
        # print(*values, sep=sep, end=end)  # for testing

    return func


# default log function
log = logger()


# *** retry decorator ***
# defaults
MAX = 3
WAIT = 45


def wait_for_internet(logger=log):
    """check/wait for internet connection, log time waited & return bool whether waited."""
    log = logger
    waited = False
    start = time_now()
    while True:
        try:
            requests.head(random.choice(['http://www.google.com', 'http://www.bing.com', 'http://www.aol.com']))
            if waited:
                log("Internet connection went down for", time_now() - start)
            return waited
        except:
            waited = True
            sleep(15)


def retry_options(max=MAX, wait=WAIT, raise_error=True, on_error=None):
    """convenience function for adding custom retry settings to class
    usage: self.retry = retry_options(max=None)
    """
    return {'max': max, 'wait': wait, 'raise_error': raise_error, 'on_error': on_error}


def retry(max=MAX, wait=WAIT, raise_error=True, on_error=None):
    """retry decorator factory
    :param max: max attempts to retry, set to None for infinite retries
    :param wait: interval to wait between tries, in seconds
    :param raise_error: bool, raise exceptions or ignore on last attempt (exception will not be logged!).
    :param on_error: function or method to call before retrying (won't be called on last attempt)
    """
    logger = log  # to avoid UnboundLocalError

    # actual decorator
    def decorate(func):

        # new function
        @functools.wraps(func)
        def new(*args, **kwargs):
            nonlocal max, wait, raise_error, on_error

            # check if custom options specified in class
            cls = args[0] if args else None

            # prefer retry options dict from class
            if cls and hasattr(cls, 'retry') and isinstance(cls.retry, dict):
                max = args[0].retry.get('max', max)
                wait = args[0].retry.get('wait', wait)
                raise_error = args[0].retry.get('raise_error', raise_error)
                on_error = args[0].retry.get('on_error', on_error)

            # prefer log function from class
            log = cls.log if cls and hasattr(cls, 'log') and inspect.isfunction(cls.log) else logger

            # max of 0 == do not attempt
            if max == 0:
                return

            # retry x times
            attempt = 1
            while True:

                try:
                    return func(*args, **kwargs)

                except:
                    if attempt == max and raise_error:
                        raise
                    elif attempt == max:
                        # note: on last attempt, exception will not be logged.
                        return

                    log("[attempt #" + str(attempt) + "]", stacktrace())

                    if on_error:
                        # call on_error function
                        on_error()

                # check internet connection. if waited for internet, don't sleep
                if not wait_for_internet(logger=log):
                    sleep(wait)

                attempt += 1

        return new

    return decorate