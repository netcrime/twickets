# Information

Program that scans twickets for new tickets based on criteria
and notifies receiver by email about results.

Author: Povilas Kudriavcevas, povilas.kudriavcevas@gmail.com

# Usage
* Python 3 required to run. You can get it from www.python.org

1. Download/Clone the source

2. Install required packages 'pip install -r requirements.txt'

3. Edit settings.ini

4. Run the code by typing 'python3 run.py'



# Technical information

## Breaked example URL
---

https://www.twickets.live/services/catalogue?
api_key=83d6ec0c-54bb-4da3-b2a1-f3cb47b984f1&
count=10&
q=eventName~=sheeran,
regionCode=AUNSW,
countryCode=AU


## Example URL
---
https://www.twickets.live/services/catalogue?api_key=83d6ec0c-54bb-4da3-b2a1-f3cb47b984f1&count=10&q=eventName~=sheeran,regionCode=AUNSW,countryCode=AU


## Codes for regions
---

AUNSW - New South Wales
AUWA - West Australia
AUVIC - Victoria
AUTAS - Tasmania
AUSA - South Australia
AUQLD - Queensland
AUNT - Northern Territory
AUACT - ACT