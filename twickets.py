import time
import requests

from shortcuts import retry


class Twickets:
    """Class that handles Twickets API communication"""
    def __init__(self, count, search_query, region, min_tickets, logging):
        self.base_url = 'https://www.twickets.live/services/catalogue'
        self.url_params = '?api_key=%s&count=%s&q=eventName~=%s,regionCode=%s,countryCode=%s'
        self.api_key = '83d6ec0c-54bb-4da3-b2a1-f3cb47b984f1'
        self.count = count
        self.search_query = search_query
        self.region_code = region
        self.country_code = 'AU'
        self.min_tickets = min_tickets
        self.logging = logging

        self.url = self.base_url + self.url_params % (self.api_key,
                                                      self.count,
                                                      self.search_query,
                                                      self.region_code,
                                                      self.country_code)
    
    def _reconstruct_url(self):
        """Use this method to reconstruct URL if parameters change"""
        self.url = self.base_url + self.url_params % (self.api_key,
                                                      self.count,
                                                      self.search_query,
                                                      self.region_code,
                                                      self.country_code)

    @retry(max=5, wait=7)
    def _make_request(self):
        """Returns response object from Twickets"""
        return requests.get(self.url)
        
    
    def get_tickets(self, known_tickets):
        """Returns ads with more then x tickets"""
        response = self._make_request()
        ticket_list = []
        if response.status_code == 200:
            for item in response.json()['responseData']:
                if item['catalogBlockSummary']:
                    if not item['catalogBlockSummary']['blockId'] in known_tickets:
                        if item['catalogBlockSummary']['ticketQuantity'] >= self.min_tickets:
                            try:
                                ticket = Ticket(
                                    quantity=item['catalogBlockSummary']['ticketQuantity'],
                                    price = item['catalogBlockSummary']['twicketsFeePerTicket']['amountInCents'],
                                    created = item['catalogBlockSummary']['created'],
                                    block_id = item['catalogBlockSummary']['blockId'],
                                    section = item['catalogBlockSummary'].get('section', ''),
                                    row = item['catalogBlockSummary'].get('row', '')
                                )
                            except Exception as e:
                                self.logging.error('ERROR: %s' % e)
                                continue
                            ticket_list.append(ticket)
            return ticket_list
        else:
            self.logging.warning('Received a bad response. Response code is %s' % response.status_code)
            return []


class Ticket:
    """Single ticket object"""
    def __init__(self, quantity, price, created, block_id, section, row):
        self.quantity = quantity
        self.price = price
        self.block_id = block_id
        self.section = section
        self.row = row

        self.base_buy = 'https://www.twickets.live/block/%s,%s'
        self.buy_link = self.base_buy % (self.block_id, self.quantity)
    
    def info(self):
        return """Block ID: %s Quantity: %s Buy Link: %s """ % (self.block_id,
                                                                self.quantity,
                                                                self.buy_link)