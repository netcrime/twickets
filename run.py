"""
Program that scans twickets for new tickets based on criteria
and notifies receiver by email about results.

@ Povilas Kudriavcevas
@ Povilas.Kudriavcevas@gmail.com
"""

import configparser
import logging
import time

from twickets import Twickets
from notifi import EmailClient


def general():
    """Main function controling program workflow"""
    # Config parsing
    config = configparser.ConfigParser()
    config.read('settings.ini')
    delay_sec = int(config['Base']['delay_sec'])
    search = config['Base']['search']
    min_tickets = int(config['Base']['min_tickets'])
    region = config['Base']['region']
    
    logfile = config['Base']['logfile']

    email = config['Email']['email']
    password = config['Email']['password']
    port = config['Email']['port']
    smtp = config['Email']['server']
    receiver = config['Email']['receiver']

    # Logging settings
    log_settings = {
        'level': logging.INFO,
        'format': '%(asctime)s:%(levelname)s: %(message)s'
    }
    if logfile:
        log_settings['filename'] = logfile
    logging.basicConfig(**log_settings)

    notifi = EmailClient(email, password, port, smtp, receiver, logging)

    # Actual workflow of the program
    twickets = Twickets(
        count=100,
        search_query=search,
        region=region,
        min_tickets=min_tickets,
        logging=logging
    )

    known_tickets = []

    while True:
        logging.info('Scanning Twickets for new tickets.')
        tickets = twickets.get_tickets(known_tickets)
        if tickets:
            logging.info('Found %s new tickets!' % len(tickets))
            notifi.send_email(tickets)
            for ticket in tickets:
                logging.info(ticket.info())
                known_tickets.append(ticket.block_id)
        else:
            logging.info('No new tickets found.')
        logging.info('Sleeping for %s seconds' % delay_sec)
        time.sleep(delay_sec)

if __name__ == '__main__':
    general()
